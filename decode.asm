extern puts
extern printf

section .data
filename: db "./input.dat",0
inputlen: dd 2263
fmtstr: db "Key: %d",0xa,0

section .text
global main

; TODO: define functions and helper functions

; finds the length of an array
strlen:
    push ebp
    mov ebp, esp
    xor eax, eax
    mov ecx, 100
    mov edi, dword [ebp + 8]
    repnz scasb
    mov eax, 100
    sub eax, ecx
    dec eax
    leave
    ret

; xor string and key byte by byte
xor_strings:
	push ebp
	mov ebp, esp
	mov eax, dword [ebp + 8]
	mov ebx, dword [ebp + 12]

not_end_of_string:
	mov byte dl, [ebx]
	xor byte [eax], dl
	inc eax
	inc ebx
	cmp byte [eax], 0
	jne not_end_of_string 
	leave
	ret

; xor between a byte and the previous one in the message
rolling_xor:
	push ebp
	mov ebp, esp
	mov eax, dword [ebp + 8]

not_beginning_of_string:
	mov byte dl, [eax - 1]
	xor byte [eax], dl
	dec eax
	cmp byte [eax - 1], 0
	jne not_beginning_of_string
	leave
	ret

; converts a string to a stream of bytes
convert_string_to_hex:
	push ebp
	mov ebp, esp
	mov ebx, dword [ebp + 8]
	xor edx, edx
iterate:
	mov dl, [ebx]
	cmp dl, 97
	jb is_digit
	sub dl, 39
is_digit:
	sub dl, 48
	mov [ebx], dl 
	inc ebx
	cmp byte [ebx], 0
	jne iterate
	leave
	ret

; takes two characters representing numbers and transform them in a byte
create_bytes:
	push ebp
	mov ebp, esp
	mov eax, dword [ebp + 8]
	mov ebx, dword [ebp + 12]
	xor ecx, ecx
	xor edx, edx
compress:
	mov dl, [eax + ecx * 2]
	shl dl, 4
	add dl, [eax + ecx * 2 + 1]
	mov [eax + ecx], dl
	inc ecx
	cmp ecx, 35
	jne compress
	mov ebx, dword [ebp + 8]
fill_with_zero:
	mov byte [ebx + ecx], 0
	inc ecx
	cmp byte [ebx + ecx], 0
	jne fill_with_zero
	leave
	ret

; xor between two strings
xor_hex_strings:
	push ebp
	mov ebp, esp
	mov ebx, dword [ebp + 8]
	mov edx, dword [ebp + 12]
	push edx
	push ebx
	call xor_strings
	add esp, 8
	leave
	ret

; takes characters and transforms them into indexes from the base32 table
turn_char_into_index:
	push ebp
	mov ebp, esp
	xor edx, edx
	mov ebx, dword [ebp + 8]
continue_indexing:
	mov byte dl, [ebx]
	cmp dl, 61
	je exit
	jb digit
	sub dl, 41
digit:
	sub dl, 24
	shl dl, 3
	mov byte [ebx], dl
	inc ebx
	cmp byte [ebx], 0
	jne continue_indexing
exit:
	leave
	ret

; concatenates streams of 5 bits to decode the string from base32
; there are 8 possibilities of combining bytes in streams of 5 bits
base32decode:
	push ebp
	mov ebp, esp
	xor ecx, ecx
	xor edx, edx
	mov ebx, dword [ebp + 8]
	mov eax, ebx
rearrange_bits:
	mov dl, [ebx]
	cmp ecx, 0
	je mask0
	cmp ecx, 1
	je mask1
	cmp ecx, 2
	je mask2
	cmp ecx, 3
	je mask3
	cmp ecx, 4
	je mask4
	cmp ecx, 5
	je mask5
	cmp ecx, 6
	je mask6
	cmp ecx, 7
	je mask7
continue:
	inc ebx
	cmp byte [ebx], 61
	jne rearrange_bits
	sub ebx, eax
	mov ecx, ebx
	leave
	ret
mask0:
	mov [eax], dl
	inc ecx
	jmp continue
mask1:
	and dl, 0xe0
	shr dl, 5
	add [eax], dl
	inc eax
	mov dh, [ebx]
	shl dh, 3
	mov [eax], dh
	inc ecx
	jmp continue
mask2:
	shr dl, 2
	add [eax], dl
	inc ecx
	jmp continue
mask3:
	and dl, 0x80
	shr dl, 7
	add [eax], dl
	inc eax
	mov dh, [ebx]
	shl dh, 1
	mov [eax], dh
	inc ecx
	jmp continue
mask4:
	and dl, 0xf0
	shr dl, 4
	add [eax], dl
	inc eax
	mov dh, [ebx]
	shl dh, 4
	mov [eax], dh
	inc ecx
	jmp continue
mask5:
	shr dl, 1
	add [eax], dl
	inc ecx
	jmp continue
mask6:
	and dl, 0xc0
	shr dl, 6
	add [eax], dl
	inc eax
	mov dh, [ebx]
	shl dh, 2
	mov [eax], dh
	inc ecx
	jmp continue
mask7:
	shr dl, 3
	add [eax], dl
	inc eax
	xor ecx, ecx
	jmp continue

; function that gets the decrypted message and verifies if "force" exists in it
find_force:
	push ebp
	mov ebp, esp
	mov edx, dword [ebp + 8]
	xor ebx, ebx
	xor eax, eax
continue_until_found:
	cmp byte [edx + ebx], 0x66
	jne inc_ebx
	inc ebx
	cmp byte [edx + ebx], 0
	je end_verification
	cmp byte [edx + ebx], 0x6f
	jne inc_ebx
	inc ebx
	cmp byte [edx + ebx], 0
	je end_verification
	cmp byte [edx + ebx], 0x72
	jne inc_ebx
	inc ebx
	cmp byte [edx + ebx], 0
	je end_verification
	cmp byte [edx + ebx], 0x63
	jne inc_ebx
	inc ebx
	cmp byte [edx + ebx], 0
	je end_verification
	cmp byte [edx + ebx], 0x65
	jne inc_ebx
	mov eax, 1
	jmp end_verification
inc_ebx:
	inc ebx
	cmp byte [edx + ebx], 0
	jne continue_until_found
end_verification:
	leave
	ret

; function that puts in al all values from 0x01 to 0xff and decides which is the key
find_key_bruteforce:
	push ebp
	mov ebp, esp
	mov ecx, dword [ebp + 8]
	mov edx, dword [ebp + 12]
	xor eax, eax
	xor ebx, ebx
	mov al, 1

xor_all_string:
	mov byte ah, [ecx + ebx]
	xor ah, al
	mov byte [edx + ebx], ah
	inc ebx
	cmp byte [ecx + ebx], 0
	jne xor_all_string
	mov byte [edx + ebx], 0
	push eax
	push edx
	push edx
	call find_force
	add esp, 4
	cmp eax, 1
	je found	
	pop edx
	pop eax
	xor ebx, ebx
	inc al
	cmp al, 255
	jb xor_all_string
found:
	pop edx
	pop eax
	mov ah, 0
	leave
	ret

; applies xor on the encrypted message with the key found
bruteforce_singlebyte_xor:
	push ebp
	mov ebp, esp
	mov dword ebx, [ebp + 8]
	mov dword edx, [ebp + 12]
repeat_xor:
	xor [ebx], dl
	inc ebx
	cmp byte [ebx], 0
	jne repeat_xor
	leave
	ret

main:
    push ebp
    mov ebp, esp
    sub esp, 2300
    
    ; fd = open("./input.dat", O_RDONLY);
    mov eax, 5
    mov ebx, filename
    xor ecx, ecx
    xor edx, edx
    int 0x80
    
	; read(fd, ebp-2300, inputlen);
	mov ebx, eax
	mov eax, 3
	lea ecx, [ebp-2300]
	mov edx, [inputlen]
	int 0x80

	; close(fd);
	mov eax, 6
	int 0x80

	; all input.dat contents are now in ecx (address on stack)

	; TASK 1: Simple XOR between two byte streams
	mov edx, ecx
	
	; find the length of the message
    push ecx
    call strlen
    add esp, 4

    ; save the length of the first message
    push eax

	; TODO: compute addresses on stack for str1 and str2
    mov ecx, edx
    inc eax
    add eax, ecx

    ; TODO: XOR them byte by byte
    push eax
    push ecx
    call xor_strings
    add esp, 8

    ; save ecx
    push ecx

	; Print the first resulting string
	push ecx
	call puts
	add esp, 4

	; restore ecx
	pop ecx

	; TASK 2: Rolling XOR
    ; TODO: compute address on stack for str3
	pop eax
	inc eax
	add eax, eax
	add ecx, eax

	mov edx, ecx

	;find the length of the message
	push ecx
	call strlen
	add esp, 4

	; save the length of the message
    push eax

	; eax will point to the last element of the message
	mov ecx, edx
	add eax, ecx
	dec eax
	
	; TODO: implement and apply rolling_xor function
	push eax
	call rolling_xor
	add esp, 4

	; save ecx
    push ecx

	; Print the second resulting string
	push ecx
	call puts
	add esp, 4

	; restore ecx
	pop ecx

	; TASK 3: XORing strings represented as hex strings
	; TODO: compute addresses on stack for strings 4 and 5
	pop eax
	inc eax
	add ecx, eax

	mov edx, ecx

	;find the length of the message
	push ecx
	call strlen
	add esp, 4

	;save the length of the message
	push eax

	; save half of the length for create_bytes function
	mov ebx, eax
	shl ebx, 1
	push ebx

	; compute the address of the key
	mov ecx, edx
	inc eax
	add eax, ecx

	; convert the message
	push ecx
	call convert_string_to_hex
	add esp, 4
	; convert the key
	push eax
	call convert_string_to_hex
	add esp, 4

	; get the value that represents half the length
	pop ebx

	; save eax, ecx and ebx registers because they will be modified by the create_bytes function
	push ecx
	push eax

	push ebx

	;create bytes for message
	push ebx
	push ecx
	call create_bytes
	add esp, 8

	pop ebx

	; get address of key
	pop eax
	; save the value of the address of the key
	push eax
	;call function create_bytes
	push ebx
	push eax
	call create_bytes
	add esp, 8

	; restore the addresses of the message and the key
	pop eax
	pop ecx

	; TODO: implement and apply xor_hex_strings
	push eax
	push ecx
	call xor_hex_strings
	add esp, 8

	; save the address in ecx
	push ecx

	; Print the third string
	push ecx
	call puts
	add esp, 4

	; restore ecx
	pop ecx

	; restore the length of the message
	pop eax

	
	; TASK 4: decoding a base32-encoded string
	; TODO: compute address on stack for string 6
	inc eax
	add eax, eax
	add ecx, eax

	; strlen modifies ecx
	push ecx

	push ecx
	call strlen
	add esp, 4

	pop ecx

	; save the length of the message
	push eax

	push ecx
	call turn_char_into_index
	add esp, 4

	; save the value of ecx
	push ecx

	; TODO: implement and apply base32decode
	push ecx
	call base32decode
	add esp, 4

	; restore the value of ecx
	pop ecx

	; save ecx
	push ecx

	; Print the fourth string
	push ecx
	call puts
	add esp, 4

	; restore ecx
	pop ecx

	; restore the length of the message
	pop eax

	; TASK 5: Find the single-byte key used in a XOR encoding
	; TODO: determine address on stack for string 7

	; ecx will point to the first equal sign of the string
	inc eax
	add ecx, eax

	push ecx

	; find how many equal signs are in the string
	push ecx
	call strlen
	add esp, 4

	pop ecx

	; determine the address of the next string
	inc eax
	add ecx, eax

	push ecx

	push ecx
	call strlen
	add esp, 4

	pop ecx

	; save space for a buffer where xor operations will be made
	inc eax
	
	push ebp
	mov ebp, esp
	sub esp, eax
	sub ebp, eax
	lea edx, [ebp]
	add ebp, eax

	; save message and buffer
	push ecx
	push edx

	; find the key
	push edx
	push ecx
	call find_key_bruteforce
	add esp, 8

	; restore message and buffer
	pop edx
	pop ecx

	; TODO: implement and apply bruteforce_singlebyte_xor
	push eax
	push ecx
	call bruteforce_singlebyte_xor
	add esp, 8

	; save message
	push ecx

	push eax

	; Print the fifth string and the found key value
	push ecx
	call puts
	add esp, 4

	pop eax

	push eax
	push fmtstr
	call printf
	add esp, 8

	; restore the message
	pop ecx

	; restore the stack
	leave

	; TASK 6: Break substitution cipher
	; TODO: determine address on stack for string 8

	push ecx

	push ecx
	call strlen
	add esp, 4

	pop ecx

find_string8:
	inc ecx
	cmp byte [ecx], 0
	jne find_string8
	inc ecx

	; TODO: implement break_substitution
	;push substitution_table_addr
	;push addr_str8
	;call break_substitution
	;add esp, 8

	; Print final solution (after some trial and error)
	;push addr_str8
	;call puts
	;add esp, 4

	; Print substitution table
	;push substitution_table_addr
	;call puts
	;add esp, 4

	; Phew, finally done
    xor eax, eax
    leave
    ret
